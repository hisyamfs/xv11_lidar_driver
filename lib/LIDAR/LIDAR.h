#ifndef MBED_LIDAR_H
#define MBED_LIDAR_H

#include "mbed.h"

/** A Library used to read data from Low-cost NEATO LIDAR
 *
 * Example:
 * @code
 * #include "mbed.h"
 * #include "LIDAR.h"
 * 
 * int main() {
 *   // The tx pin is PC_10
 *   // The rx pin is PC_11
 *   // The PWM pin to control motor is PA_15
 *   LIDAR lidar (PC_10, PC_11, PA_15);
 *  
 *   // Activate serial to PC for debugging
 *   Serial pc(USBTX, USBRX);
 *
 *   // Set duty cycle for motor PWM
 *   lidar.SetDuty(0.25);
 *
 *   // Start data aquisition
 *   lidar.StartData();
 *
 *   while (1) {
 *       // Aquire LIDAR data from angle 0, 45, 90, 135... 315
 *       // Then send it to serial PC with 9600 baud
 *       data = lidar.GetData(i);
 *       speed = lidar.GetSpeed();
 *       pc.printf("Spd=%.1f; D[%d]=%.1f\n", speed, i, data);
 *       i += 45;
 *       if(i >= 360) i = 0;
 *       wait(0.5);
 *   }
 * }
 * @endcode
 */
class LIDAR {

public:
    /** Create an LIDAR object connected to the specified serial port and 
     *  PWM port for LIDAR motor control
     *
     * @param tx Transmit pin
     * @param rx Receive Pin
     * @param motor_pwm PWM pin for motor control 
     *
     * @note
     *   The LIDAR will be initiated with 1kHz PWM with 0% duty-cycle
     *   and 115200 baud (standard baud for NEATO LIDAR)
     */
    LIDAR(PinName tx, PinName rx, PinName motor_pwm);
    
    /** Start data aquisition after setting up other initial parameter
     *  such as motor duty cycle and period.
     *
     * @note
     *   This function will activate Rx interrupt each time data is received
     */
    void StartData(void);
    
    /** Stop data aquisition
     *
     * @note
     *   This function will deactivate Rx Interrupt
     */
    void StopData(void);
    
    /** Set the PWM duty cycle precentage for LIDAR motor
     *
     * @param duty The duty cycle ranged from 0.0(0%) to 1.0(100%) 
     *
     */
    void SetPWMDuty(float duty);
    
    /** Set the PWM duty cycle for LIDAR motor in microseconds. The
     *  generated frequency will be f = 1000000/period
     *
     * @param microseconds The PWM period in microseconds 
     *
     */
    void SetPWMPeriodUs(int microseconds);
    
    /** Obtain the distance data read by LIDAR in certain angle
     *
     * @param degree The angle where the data want to be read, it is integer ranged from 0-359  
     *
     * @return Distance data in cm
     */
    float GetData(int degree);

    /** Mengecek apakah data pada sudut <degree> valid atau tidak
     *
     * @param degree The angle where the data is to be read, with value in integer from 0 to 359
     *
     * @return 1 if data at degree is invalaid, else 0
     * 
    */
    bool IsDataInvalid(int degree);

    bool GetStrengthFlag(int degree);
    
    /** Obtain the rotation speed of LIDAR motor
     * 
     * @return Speed data in rpm
     */
    float GetSpeed(void);

    short GetIntensity(int degree);

    char GetByte0(int degree);

    char GetByte1(int degree);

    uint8_t GetChecksuml();

    uint8_t GetChecksumh();

private:
    Serial _lidar;
    PwmOut _motor;
    
    short _data[360];
    char* _data_ptr;
    unsigned short _speed;
    char* _speed_ptr;
    short _intensity[360];
    char* _intensity_ptr;
    bool _invalid_flag[360];
    bool _strength_flag[360];
    
    char _fsm_state;
    char _fsm_count;
    unsigned short _fsm_angle;
    
    uint8_t checksum_l;
    uint8_t checksum_h;
    void data_parser(void);
};

#endif